; ---------------------------------------------
; pipe1.asm
; ---------------------------------------------
;
; This example will create a pipe from the
; parent process to the child process, using
; pipe(). Then it sends a message from the
; child process to the parent process and dis-
; play it.
;

SECTION .data
    msg1    db      'Hello World', 0xA, 0x0

SECTION .bss

    fds:
    fd1     resd    1,          ; fd1 for read end of the pipe
    fd2     resd    1,          ; fd2 for write end of the pipe

    tmp     resb    13,         ; reserve 13 bytes of memory

SECTION .text
global _start
_start:

    ; Make pipe
    mov     rax, 22
    mov     rdi, fds
    syscall

    ; Fork process
    mov     rax, 57
    syscall
    cmp     rax, 0
    jz      child

    ; Wait until the child process has finished.
    mov     rdi, 0
    mov     rax, 61
    xor     rsi, rsi
    xor     rdx, rdx
    xor     r10, r10
    syscall

    ; Read msg1 from fd1
    mov     rax, 0
    mov     rdi, [fd1]
    mov     rdx, 12
    mov     rsi, tmp
    syscall

    ; Print message
    mov rax, 1
    mov rdi, 1
    mov rsi, tmp
    mov rdx, 12
    syscall

    ; Quit
    mov rax, 60
    mov rdi, 0
    syscall

child:
    ; Write "Hello World", 0xA, 0x0 using fd2
    mov     rax, 1
    mov     rdi, [fd2]
    mov     rsi, msg1
    mov     rdx, 12
    syscall

