# nasm-examples-pipe-linux-64bit


## Getting started

I wrote these three source code examples while learning to program with nasm (Netwide Assembler). These sample applications work with 64-bit Linux operating systems.

## Required

- A 64bit Linux operating system
- Netwide Assembler (NASM)

## Using

Each directory contains a source code file and a _Makefile_ with which it can be compiled. However, if you prefer to compile your source code manually, then run the following commands:

For example: 
```
cd pipe1
nasm -g -f elf64 pipe1.asm
ld pipe1.o -o pipe1
./pipe1
```

## Description of the source code files

- pipe1.asm: This example will create a pipe from the parent process to the child process, using pipe(). Then it sends a message from the child process to the parent process and display it.

- pipe2.asm: This example creates a pipe, using pipe() and sends the message _'Hello World'_ to the pipe, read from it and display/print the message. This example is my version, written in nasm, of this example from this link, that is written in FASM: https://www.linuxquestions.org/questions/programming-9/how-can-i-use-pipes-in-assembly-4175535653/

- pipe3.asm: This example will create a pipe from the parent process to the child process, using pipe(), runs an external command (```/bin/echo 'Hello World'```), copy the result to the memory and display/print it.

## License
GPLv3

## Search terms
_nasm, nasm example pipe linux, run external commands with nasm, get answer from external command nasm, get result from external command, nasm howto pipe, nasm fork(), SYS_PIPE, SYS_FORK, SYS_DUP2, nasm dup2 fork pipe, use pipe nasm, get result external command nasm linux_
