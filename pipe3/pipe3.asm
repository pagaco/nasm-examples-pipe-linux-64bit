; ---------------------------------------------------
; pipe3.asm
; ---------------------------------------------------
;
; This example will create a pipe from the parent
; process to the child process, using pipe(), runs an
; external command (/bin/echo 'Hello World') using
; execve(), copy the result in the memory and print 
; the result.
;

SECTION .data

    ; Important: The strings must be null-terminated
    bin     db '/bin/echo', 0x0
    arg1    db 'Hello world', 0x0
    args    dq bin, arg1, 0x0

SECTION .bss

    fds:
    fd1     resd    1,      ; fd1 for read end of the pipe
    fd2     resd    1,      ; fd2 for write end of the pipe

    tmp     resb    12,     ; reserve 12 bytes of memory

SECTION .text
global _start
_start:

    ; Make pipe
    mov     rax, 22
    mov     rdi, fds
    syscall

    ; Fork process
    mov     rax, 57
    syscall
    cmp     rax, 0
    jz      child

    ; Wait until the child process is finished
    mov     rdi, 0
    mov     rax, 61
    xor     rsi, rsi
    xor     rdx, rdx
    xor     r10, r10
    syscall

    ; Close fd2 because parent process does read only from pipe
    mov     rax, 3
    mov     rdi, [fd2]
    syscall

    ; Read from fd1 and write content to tmp
    mov     rax, 0
    mov     rsi, tmp
    mov     rdi, [fd1]
    mov     rdx, 12
    syscall

    ; Print result from external command
    mov rax, 1
    mov rdi, 1
    mov rsi, tmp
    mov rdx, 12
    syscall

    ; Quit
    mov rax, 60
    mov rdi, 0
    syscall

child:
    ; Close fd1 because child process does write only to pipe
    mov     rax, 3
    mov     rdi, [fd1]
    syscall

    ; Duplicate filedescriptor to delete the close-on-exec flag and to provide the duplicated filedescriptor for execve()
    mov     rax, 33
    mov     rdi, [fd2]
    mov     rsi, 1
    syscall

    ; Close fd2 because is no longer needed
    mov     rax, 3
    mov     rdi, [fd2]
    syscall

    ; Execute the external command
    mov     rax, 59               
    mov     rdi, bin
    mov     rsi, args
    xor     rdx, rdx
    syscall
