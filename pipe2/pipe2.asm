; ---------------------------------------------
; pipe2.asm
; ---------------------------------------------
;
; This example creates a pipe, using pipe() and
; sends the message 'Hello World' to the pipe,
; read from it and display/print the message.
;
;

SECTION .data

    msg1    db      'Hello World', 0xA, 0x0

SECTION .bss

    fds:
    fd1     resd    1,          ; fd1 for read end of the pipe
    fd2     resd    1,          ; fd2 for write end of the pipe

    tmp     resb    13,         ; reserve 13 bytes of memory

SECTION .text
global _start
_start:

    ; Make pipe
    mov     rax, 22
    mov     rdi, fds
    syscall

    ; Send "Hello World", 0x0 to fd2
    mov     rax, 1
    mov     rdi, [fd2]
    mov     rsi, msg1
    mov     rdx, 12
    syscall

    ; Read msg1 from fd1
    mov     rax, 0
    mov     rdi, [fd1]
    mov     rdx, 13
    mov     rsi, tmp
    syscall

    ; Print message
    mov rax, 1
    mov rdi, 1
    mov rsi, tmp
    mov rdx, 12
    syscall

    ; Quit
    mov rax, 60
    mov rdi, 0
    syscall

